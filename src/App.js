import 'antd/dist/antd.css';
import './App.less'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Welcome from './pages/Welcome'
import PageNotFound from './pages/PageNotFound';

import OnlineAppHome from './pages/OnlineApplication'


function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={Welcome} />
          <Route path="/online_application" component={OnlineAppHome} />
          <Route component={PageNotFound} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
