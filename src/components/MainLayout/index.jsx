import React from 'react'
import { Avatar, Layout, Space } from 'antd'
import { ArrowLeftOutlined } from '@ant-design/icons'
import { withRouter, useHistory } from 'react-router-dom'

import './index.less'

const { Header, Content, Footer } = Layout; 

function MainLayout({ children, location, pagename }) {
    const history = useHistory();

    console.log(history)

    return (
        <Layout className="layout">
            <Header className="header">
                <Space>
                    {location.pathname === "/" ? <></> : <ArrowLeftOutlined onClick={() => history.push("/")} />}
                    {pagename}
                </Space>
                <div className="user">
                    {
                        location.state?.loggedIn ? (<Space>
                            Kelvin Vegilla
                            <Avatar />
                        </Space>) : (<></>)
                    }
                </div>
            </Header>
            <Content className="content">
                {children}
            </Content>
            <Footer className="footer">
                {process.env.REACT_APP_WEBSITE_NAME} &#169; 2021 {process.env.REACT_APP_COMPANY_NAME}
            </Footer>
        </Layout>
    )
}

export default withRouter(MainLayout)
