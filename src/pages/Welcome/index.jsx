import React from 'react'
import { Col, Row, Card, Image, Typography } from 'antd'
import { useHistory } from 'react-router-dom'

import MainLayout from '../../components/MainLayout'
import OnlineApplication from '../../assets/online_application.png'
import './index.less'

const { Title } = Typography;

const modules = [
    {
        link: "/online_application/login",
        image: OnlineApplication,
        name: "Online Application"
    },
    {
        link: "/faculty/login",
        image: OnlineApplication,
        name: "Faculty"
    },
    {
        link: "/student/login",
        image: OnlineApplication,
        name: "Student/Alumni"
    },
    {
        link: "/finance/login",
        image: OnlineApplication,
        name: "Finance"
    },
    {
        link: "/registrar/login",
        image: OnlineApplication,
        name: "Registrar"
    },
    {
        link: "/cashier/login",
        image: OnlineApplication,
        name: "Cashier"
    },
]


function Welcome() {
    const history = useHistory()
    return (
        <MainLayout pagename="Welcome">
            <div className="container">
                <Row gutter={[16, 16]} justify="center" align="middle">
                    {
                        modules.map((module, index) => (
                            <Col xs={24} sm={24} md={12} lg={8}>
                                <Card
                                    hoverable
                                    onClick={() => history.push(module.link)}
                                >
                                    <div className="card-body">
                                        <Image src={module.image} width="128px" preview={false} /><br />
                                        <Title level={4}>{module.name}</Title>
                                    </div>
                                </Card>
                            </Col>  
                        ))
                    }
                </Row>
            </div>
        </MainLayout>
    )
}

export default Welcome
