import React from 'react'
import { Switch, Route } from 'react-router-dom'
import MainLayout from '../../components/MainLayout'

import Login from './Login'
import Register from './Register'
import Success from './Success'
import PageNotFound from '../PageNotFound'

function index() {
    return (
        <MainLayout pagename="Online Application">
            <Switch>
                <Route exact path="/online_application/login" component={Login} />
                <Route exact path="/online_application/register" component={Register} />
                <Route exact path="/online_application/success" component={Success} />
                <Route component={PageNotFound} />
            </Switch>
        </MainLayout>
    )
}

export default index
