import React from 'react'
import { Form, Input, DatePicker, Select, Image, Button } from 'antd'
import { UserOutlined, MailOutlined, CalendarOutlined } from '@ant-design/icons'
import { useHistory } from 'react-router-dom'

import './index.less'

const { Option } = Select;

const onFinish = (values) => {
    // history.push("/online_application/home")
    console.log('Success:', values);
};

function Register() {
    const [ form ] = Form.useForm();
    const history = useHistory();
    return (
        <div className="register-body">
            <div className="register-logo">
                <Image />
            </div>
            <div className="register-form">
                <Form 
                    form={form}
                    onFinish={onFinish}
                >
                    <Form.Item
                        name="lastname"
                        rules={[{ required: true, message: 'Please input your last name' }]}
                    >
                        <Input placeholder="Last Name" suffix={<UserOutlined className="icon-suffix" />} />
                    </Form.Item>
                    <Form.Item
                        name="firstname"
                        rules={[{ required: true, message: 'Please input your first name' }]}
                    >
                        <Input placeholder="First Name" suffix={<UserOutlined className="icon-suffix" />} />
                    </Form.Item>
                    <Form.Item
                        name="middlename"
                        rules={[{ required: true, message: 'Please input your middle name' }]}
                    >
                        <Input placeholder="Middle Name" suffix={<UserOutlined className="icon-suffix" />} />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        rules={[{ required: true, message: 'Please input your email address', type: 'email' }]}
                    >
                        <Input placeholder="Email Address" suffix={<MailOutlined className="icon-suffix" />} />
                    </Form.Item>
                    <Form.Item
                        name="birthdate"
                        rules={[{ required: true, message: 'Please input your birth date' }]}
                    >
                        <DatePicker placeholder="Birth Date" className="full-width" suffixIcon={<CalendarOutlined className="icon-suffix" />} format="MMMM DD, YYYY" />
                    </Form.Item>
                    <Form.Item
                        name="branch"
                        rules={[{ required: true, message: 'Please select branch' }]}
                    >
                        <Select placeholder="Branch">
                            <Option value="jack">Jack</Option>
                            <Option value="lucy">Lucy</Option>
                            <Option value="Yiminghe">yiminghe</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item>
                        <Button
                            className="login-fullwidth"
                            type="primary"
                            htmlType="submit"
                        >
                            Create Account
                        </Button>
                    </Form.Item>
                    <Form.Item>
                        <Button
                            className="login-fullwidth"
                            onClick={() => history.push("/online_application/login")}
                        >
                            Already have an account? Log in here.
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
}

export default Register
