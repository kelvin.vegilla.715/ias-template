import React, { useState, useEffect } from 'react'
import { Layout, Image, Form, Input, Button, Checkbox, Typography, DatePicker } from 'antd';
import { UserOutlined, LockOutlined, CalendarOutlined } from '@ant-design/icons'
import { useHistory } from 'react-router-dom';

import './index.less'

const { Content, Footer } = Layout;
const { Link } = Typography;
function Login() {
    const [ form ] = Form.useForm();
    const [, forceUpdate] = useState({});
    const history = useHistory()

    useEffect(() => {
        forceUpdate({});
    }, []);

    const onFinish = (values) => {
        history.push("/welcome")
        console.log('Success:', values);
    };

    return (
        <div className="login-container">
            <Layout>
                <Content className="login-body">
                    <div className="login-logo">
                        <Image />
                    </div>
                    <div className="login-form">
                        <Form form={form} onFinish={onFinish} >
                            <Form.Item
                                name="studentNo"
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input your student number"
                                    }
                                ]}
                            >
                                <Input suffix={<UserOutlined className="login-suffix" />} placeholder="Student Number" />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input your password"
                                    }
                                ]}
                            >
                                <Input suffix={<LockOutlined className="login-suffix" />} placeholder="Password" />
                            </Form.Item>
                            <Form.Item
                                name="birthdate"
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input your birthdate"
                                    }
                                ]}
                            >
                                <DatePicker className="login-fullwidth" suffixIcon={<CalendarOutlined className="login-suffix" />} />
                            </Form.Item>
                            <div className="login-forgot-row">
                                <Form.Item name="remember" valuePropName="checked">
                                    <Checkbox>Remember me</Checkbox>
                                </Form.Item>
                                <Form.Item>
                                    <Link>Forgot Password?</Link>
                                </Form.Item>
                            </div>
                            <Form.Item shouldUpdate>
                                {() => (
                                    <Button
                                        className="login-fullwidth"
                                        type="primary"
                                        htmlType="submit"
                                    >
                                        Login
                                    </Button>
                                )}
                            </Form.Item>
                        </Form>
                    </div>
                </Content>
                <Footer className="login-footer">{process.env.REACT_APP_WEBSITE_NAME} &#169; 2021 {process.env.REACT_APP_COMPANY_NAME}</Footer>
            </Layout>
        </div>
    )
}

export default Login
